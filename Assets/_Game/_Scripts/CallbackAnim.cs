using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallbackAnim : MonoBehaviour
{
    [SerializeField] private UnityEvent callback1;
    [SerializeField] private UnityEvent callback2;
    [SerializeField] private UnityEvent callback3;
    [SerializeField] private UnityEvent callback4;
    [SerializeField] private UnityEvent callback5;

    public void RunCallback1()
    {
        if (callback1 != null)
        {
            callback1.Invoke();
        }
    }

    public void RunCallback2()
    {
        if (callback2 != null)
        {
            callback2.Invoke();
        }
    }

    public void RunCallback3()
    {
        if (callback3 != null)
        {
            callback3.Invoke();
        }
    }

    public void RunCallback4()
    {
        if (callback4 != null)
        {
            callback4.Invoke();
        }
    }

    public void RunCallback5()
    {
        if (callback5 != null)
        {
            callback5.Invoke();
        }
    }
}
