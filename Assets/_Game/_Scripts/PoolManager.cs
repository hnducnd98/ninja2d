using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;
    public static PoolManager instance { get { return _instance; } }

    [SerializeField] private List<GameObject> objPrefabs;

    private Dictionary<NamePrefab, List<GameObject>> dictionaryObjSaved = new Dictionary<NamePrefab, List<GameObject>>();

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }

        foreach (NamePrefab item in Enum.GetValues(typeof(NamePrefab)))
        {
            dictionaryObjSaved.Add(item, new List<GameObject>());
        }
    }

    public GameObject GetObjectAvailable(NamePrefab namePrefab, Transform transParent = null)
    {
        GameObject result = null;
        var lst = dictionaryObjSaved[namePrefab];
        for (int i = 0; i < lst.Count; i++)
        {
            if (!lst[i].activeInHierarchy)
            {
                result = lst[i];
                break;
            }
        }
        if (result == null)
        {
            result = Instantiate(objPrefabs[(int)namePrefab], transform);
            lst.Add(result);
        }
        result.SetActive(true);
        if (transParent != null)
        {
            result.transform.SetParent(transParent);
        }
        return result;
    }
}

public enum NamePrefab
{
    ENEMY,
    KUNAI,
    HEALTH
}