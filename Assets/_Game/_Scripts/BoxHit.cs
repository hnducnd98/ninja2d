using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxHit : MonoBehaviour
{
    private string myTag;

    private void Start()
    {
        myTag = transform.parent.gameObject.tag;
    }

    private void OnEnable()
    {
        Invoke(nameof(HideBoxHit), 0.2f);
    }

    private void HideBoxHit()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != myTag)
        {
            if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy")
            {
                collision.GetComponent<Character>().OnHit(Const.DAMAGE_SWORD);
                gameObject.SetActive(false);
            }
        }
    }
}
