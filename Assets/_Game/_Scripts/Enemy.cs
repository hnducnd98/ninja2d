using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

public class Enemy : Character
{
    [SerializeField] private float speed = 2;
    [SerializeField] private float rangeAttack = 0.2f;
    [SerializeField] private GameObject boxHit;

    private IState<Enemy> currentState = null;
    private Rigidbody2D rb;
    private Vector2 posMove;
    private Map _map;

    private bool _isSight;
    public bool isSight
    {
        get { return _isSight; }
        set { _isSight = value; }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (currentState != null && !isDead)
        {
            currentState.OnExcute(this);
        }
    }

    public override void OnInit()
    {
        base.OnInit();
        _isSight = false;
        currentState = null;
        SetHP(Const.MAX_HEALTH_ENEMY);
        health.GetComponent<FollowByTarget>().SetTarget(transform);
        health.OnInit(Const.MAX_HEALTH_ENEMY);
        gameObject.SetActive(true);
        ChangeState(new IdleState());
    }

    public void SetMap(Map map)
    {
        _map = map;
    }

    public void GetRandomPosMove()
    {
        posMove = _map.GetRandomPosMove();
    }

    public void OnAttack()
    {
        ChangeAnim(Const.ATTACK);
        boxHit.SetActive(true);
    }

    public void OnIdleExcute(float timer)
    {
        if (_isSight && !MainController.Instance.isDead)
        {
            ChangeDirection();
            if (timer > 0.5f)
            {
                if (IsInRangeAttack())
                {
                    ChangeState(new AttackState());
                }
                else
                {
                    ChangeState(new RunState());
                }
            }
        }
        if (timer > 3)
        {
            ChangeState(new PatrolState());
        }
    }

    public void StopMove()
    {
        rb.velocity = Vector2.zero;
    }

    public override void OnHit(int damage)
    {
        base.OnHit(damage);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        Prefs.CurrentCoin += 5;
        ChangeAnim(Const.DEAD);
        Invoke(nameof(HideEnemy), 2);
    }

    public void HideEnemy()
    {
        health.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnMove()
    {
        ChangeAnim(Const.RUN);
        ChangeDirection();
        rb.velocity = transform.right * speed;
        if (!_isSight && Mathf.Abs(posMove.x - transform.position.x) <= 0.1f)
        {
            ChangeState(new IdleState());
        }
        if (isSight && IsInRangeAttack() && !MainController.Instance.isDead)
        {
            ChangeState(new AttackState());
        }
    }

    public void ChangeDirection()
    {
        var direction = Vector3Int.zero;
        var xCheck = isSight ? MainController.Instance.player.transform.position.x : posMove.x;
        if (xCheck > transform.position.x)
        {
            direction = Vector3Int.zero;
        }
        else
        {
            direction = new Vector3Int(0, 180, 0);
        }
        transform.rotation = Quaternion.Euler(direction);
    }

    public void ChangeState(IState<Enemy> state)
    {
        if (isDead) return;
        if (currentState != state && currentState != null)
        {
            currentState.OnExit(this);
        }
        currentState = state;
        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public bool IsInRangeAttack()
    {
        if (Vector2.Distance(MainController.Instance.player.transform.position, transform.position) <= rangeAttack)
        {
            return true;
        }
        return false;
    }
}
