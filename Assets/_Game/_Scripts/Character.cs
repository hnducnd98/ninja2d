using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Animator animPlayer;

    private int _hp;
    private string _currentAnimName;

    private Health _health;
    public Health health { get { return _health; } }

    public bool isDead => _hp <= 0;
    public int hp => _hp;

    public virtual void Start()
    {

    }

    public virtual void OnInit()
    {

    }

    public void SetHealth(Health health)
    {
        this._health = health;
    }

    public void SetHP(int health)
    {
        _hp = health;
    }

    public virtual void OnDespaw()
    {

    }

    public virtual void OnHit(int damage)
    {
        _hp -= damage;
        _health?.ChangeSlide(_hp);
        if (isDead)
        {
            _hp = 0;
            OnDeath();
        }
    }

    public void ChangeAnim(string animName)
    {
        if (_currentAnimName != animName)
        {
            animPlayer.ResetTrigger(animName);
            _currentAnimName = animName;
            animPlayer.SetTrigger(_currentAnimName);
        }
    }

    public virtual void OnDeath()
    {

    }
}
