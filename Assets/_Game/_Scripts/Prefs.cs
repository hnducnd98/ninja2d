using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prefs
{
    public static int CurrentCoin
    {
        get { return PlayerPrefs.GetInt("CurrentCoin", 0); }
        set { PlayerPrefs.SetInt("CurrentCoin", value); }
    }
}
