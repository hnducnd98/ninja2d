using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private Slider slideHealth;

    public void OnInit(int maxHP)
    {
        slideHealth.maxValue = maxHP;
        slideHealth.minValue = 0;
        slideHealth.value = maxHP;
        gameObject.SetActive(true);
    }

    public void ChangeSlide(int hp)
    {
        slideHealth.value = hp;
    }
}
