using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour
{
    [SerializeField] private Enemy enemy;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !enemy.isDead)
        {
            enemy.isSight = true;
            enemy.ChangeState(new RunState());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !enemy.isDead)
        {
            enemy.isSight = false;
            enemy.ChangeState(new IdleState());
        }
    }
}
