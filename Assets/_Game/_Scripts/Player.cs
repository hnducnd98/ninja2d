using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float speed = 5;
    [SerializeField] private float jumpForce = 5;
    [SerializeField] private GameObject boxHit;
    [SerializeField] private Transform transKunaiStart;

    private Vector3 directionPlayer = new Vector3();
    private bool isGrounded;
    private bool isJumping;
    private bool isInAir;
    private bool isAttack;
    private bool isDeadZone;
    private float horizontal;

    public override void Start()
    {
        base.Start();
        OnInit();
    }

    private void Update()
    {
        if (isDeadZone || isDead) return;
        isGrounded = IsGrounded();
        Jump();
        Move();
        Attack();
        Throw();
        Dead();
    }

    public override void OnInit()
    {
        base.OnInit();
        SetHP(Const.MAX_HEALTH_PLAYER);
        if (health == null)
        {
            var _health = PoolManager.instance.GetObjectAvailable(NamePrefab.HEALTH);
            SetHealth(_health.GetComponent<Health>());
            health.GetComponent<FollowByTarget>().SetTarget(transform);
        }
        health.OnInit(Const.MAX_HEALTH_PLAYER);
        MainController.Instance.isDead = false;
        isDeadZone = false;
        isAttack = false;
        transform.position = Vector3.zero;
    }

    public override void OnDeath()
    {
        base.OnDeath();
        MainController.Instance.isDead = true;
        ChangeAnim(Const.DEAD);
        rb.velocity = Vector2.zero;
        Invoke(nameof(OnInit), 1);
    }

    private void Dead()
    {
        if (transform.position.y < MainController.Instance.transDeadZone.position.y && !isDeadZone)
        {
            isDeadZone = true;
            ChangeAnim(Const.DEAD);
            Invoke(nameof(OnInit), 1);
        }
    }

    private void Move()
    {
        if (isAttack) return;
        horizontal = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        if (Mathf.Abs(horizontal) > 0.1f)
        {
            if (isGrounded)
                ChangeAnim(Const.RUN);
            directionPlayer = new Vector3(0, horizontal < 0 ? 180 : 0, 0);
            transform.rotation = Quaternion.Euler(new Vector3(0, horizontal < 0 ? 180 : 0, 0));
        }
        else if (isGrounded && !isAttack)
        {
            ChangeAnim(Const.IDLE);
        }
    }

    private bool IsGrounded()
    {
        Debug.DrawLine(transform.position, transform.position + Vector3.down * 1.1f, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1.1f, groundLayer);
        return hit.collider != null;
    }

    private void Attack()
    {
        if (!isGrounded) return;
        if (!isAttack && Input.GetKeyDown(KeyCode.C))
        {
            rb.velocity = Vector2.zero;
            ChangeAnim(Const.ATTACK);
            isAttack = true;
            boxHit.SetActive(true);
            Invoke(nameof(ResetToIdle), 0.5f);
        }
    }

    private void Throw()
    {
        if (!isGrounded) return;
        if (!isAttack && Input.GetKeyDown(KeyCode.V))
        {
            rb.velocity = Vector2.zero;
            ChangeAnim(Const.THROW);
            isAttack = true;
            Invoke(nameof(ResetToIdle), 0.5f);

            var obj = PoolManager.instance.GetObjectAvailable(NamePrefab.KUNAI);
            obj.GetComponent<Kunai>()?.OnInit(transKunaiStart.position, Vector3.one, Quaternion.Euler(directionPlayer), gameObject.tag);
        }
    }

    private void ResetToIdle()
    {
        isAttack = false;
    }

    private void Jump()
    {
        if (isAttack) return;
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb.AddForce(new Vector2(0, jumpForce));
            ChangeAnim(Const.JUMP);
            isJumping = true;
        }

        if (rb.velocity.y > 0)
        {
            isInAir = true;
        }

        if (rb.velocity.y < 0 && !isGrounded && isInAir)
        {
            ChangeAnim(Const.FALL);
        }

        if (isGrounded)
        {
            isInAir = false;
            isJumping = false;
        }

        if (!isJumping && !isGrounded && rb.velocity.y < 0 && !isInAir)
        {
            ChangeAnim(Const.FALL);
        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isDeadZone) return;
        if (collision.gameObject.tag == "Coin")
        {
            Destroy(collision.gameObject);
            Prefs.CurrentCoin++;
        }
    }
}
