using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager instance { get { return _instance; } }

    [SerializeField] private TextMeshProUGUI txtCoin;

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
    }

    private void FixedUpdate()
    {
        txtCoin.text = Prefs.CurrentCoin.ToString();
    }
}
