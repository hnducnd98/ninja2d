public class Const
{
    // trigger anim
    public const string IDLE = "idle";
    public const string ATTACK = "attack";
    public const string RUN = "run";
    public const string THROW = "throw";
    public const string JUMP = "jump";
    public const string FALL = "fall";
    public const string DEAD = "dead";

    public const int MAX_HEALTH_PLAYER = 100;
    public const int MAX_HEALTH_ENEMY = 100;
    public const int DAMAGE_SWORD = 20;
    public const int DAMAGE_KUNAI = 15;
}
