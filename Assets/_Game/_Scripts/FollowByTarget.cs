using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowByTarget : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private bool isSmooth = false;
    [SerializeField] private float speed;
    [SerializeField] private bool isDefaultOffset = false;
    [SerializeField] private Vector3 offset = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        if (!isDefaultOffset) offset = transform.position - target.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSmooth)
        {
            transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.deltaTime * speed);
        }
        else
        {
            transform.position = target.transform.position + offset;
        }
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
