using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai : MonoBehaviour
{
    [SerializeField] private float speed;

    private Rigidbody2D rb;
    private string myTag;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb.velocity = transform.right * speed;
    }

    public void OnInit(Vector2 posStart, Vector3 scale, Quaternion rotation, string myTag)
    {
        transform.position = posStart;
        transform.localScale = scale;
        transform.rotation = rotation;
        this.myTag = myTag;
        Invoke(nameof(HideObject), 5);
    }

    private void HideObject()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != myTag)
        {
            if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Player")
            {
                collision.GetComponent<Character>().OnHit(Const.DAMAGE_KUNAI);
                CancelInvoke(nameof(HideObject));
                gameObject.SetActive(false);
            }
        }
    }
}
