using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private int _damage = 10;
    [SerializeField] private float _speed = 10;
    private Vector2 _direction = new Vector2();

    public void InitBullet(Vector3 pos, Vector3 euler, Vector2 direction)
    {
        gameObject.SetActive(true);
        transform.position = pos;
        transform.rotation = Quaternion.Euler(euler);
        _direction = direction;

        Invoke(nameof(Hidebullet), 2);
    }

    private void Hidebullet()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            var enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.OnHit(_damage);
            }
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        transform.Translate(_direction * _speed * Time.deltaTime);
    }
}