using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    [SerializeField] private Transform transLimitLeft;
    [SerializeField] private Transform transLimitRight;

    [SerializeField] private GameObject objEnemy = null;

    private void Start()
    {
        OnInit();
    }

    public Vector2 GetRandomPosMove()
    {
        var xRandom = Random.Range(transLimitLeft.position.x, transLimitRight.position.x);
        return new Vector2(xRandom, transLimitLeft.position.y);
    }

    public void TurnOffEnemy()
    {
        if (objEnemy != null)
        {
            objEnemy.GetComponent<Enemy>().HideEnemy();
        }
    }

    private void OnInit()
    {
        var rand = Random.Range(1, 11);
        if (rand < 8)
        {
            MakeEnemy();
        }
    }

    private void MakeEnemy()
    {
        var posStart = GetRandomPosMove();
        objEnemy = PoolManager.instance.GetObjectAvailable(NamePrefab.ENEMY);
        objEnemy.transform.position = new Vector3(posStart.x, posStart.y, 0);
        objEnemy.transform.rotation = Quaternion.identity;
        objEnemy.transform.localScale = Vector3.one;
        objEnemy.GetComponent<Enemy>()?.SetMap(this);
        var health = PoolManager.instance.GetObjectAvailable(NamePrefab.HEALTH);
        objEnemy.GetComponent<Enemy>()?.SetHealth(health.GetComponent<Health>());
        objEnemy.GetComponent<Enemy>()?.OnInit();
    }
}
