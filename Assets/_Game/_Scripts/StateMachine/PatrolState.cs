using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IState<Enemy>
{
    public void OnEnter(Enemy character)
    {
        character.GetRandomPosMove();
    }

    public void OnExcute(Enemy character)
    {
        character.OnMove();
    }

    public void OnExit(Enemy character)
    {
        
    }
}
