using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState<Enemy>
{
    private float timer = 0f;

    public void OnEnter(Enemy character)
    {
        character.StopMove();
        character.ChangeAnim(Const.IDLE);
        timer = 0f;
    }

    public void OnExcute(Enemy character)
    {
        timer += Time.deltaTime;
        character.OnIdleExcute(timer);
    }

    public void OnExit(Enemy character)
    {

    }
}
