using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunState : IState<Enemy>
{
    public void OnEnter(Enemy character)
    {
        character.isSight = true;
    }

    public void OnExcute(Enemy character)
    {
        character.OnMove();
    }

    public void OnExit(Enemy character)
    {
        
    }
}
