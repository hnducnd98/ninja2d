using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IState<Enemy>
{
    private float timer = 0;

    public void OnEnter(Enemy character)
    {
        timer = 0f;
        character.StopMove();
        character.OnAttack();
    }

    public void OnExcute(Enemy character)
    {
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            character.ChangeState(new IdleState());
        }
    }

    public void OnExit(Enemy character)
    {

    }
}