using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState<T>
{
    public void OnEnter(T character);
    public void OnExcute(T character);
    public void OnExit(T character);
}
