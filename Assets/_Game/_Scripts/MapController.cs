using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    private float widthScreen;
    [SerializeField] private Transform transMapMiddle;
    [SerializeField] private List<Transform> transMap;
    [SerializeField] private List<Transform> transMapPrefabs;
    [SerializeField] private Transform transMapParent;

    private void Start()
    {
        widthScreen = 18f;
        Camera.main.orthographicSize = (widthScreen / 2) / Camera.main.aspect;
    }

    private void LateUpdate()
    {
        CreateNewMaps();
    }

    private void CreateNewMaps()
    {
        var plus = widthScreen / 2;
        var isRight = true;
        var isBornNewMap = false;
        if (MainController.Instance.player.transform.position.x > transMapMiddle.position.x + plus)
        {
            transMap[0].GetComponent<Map>()?.TurnOffEnemy();
            Destroy(transMap[0].gameObject);
            transMap.RemoveAt(0);
            isRight = true;
            isBornNewMap = true;
        }
        else if (MainController.Instance.player.transform.position.x < transMapMiddle.position.x - plus)
        {
            transMap[transMap.Count - 1].GetComponent<Map>()?.TurnOffEnemy();
            Destroy(transMap[transMap.Count - 1].gameObject);
            transMap.RemoveAt(transMap.Count - 1);
            isRight = false;
            isBornNewMap = true;
        }
        if (isBornNewMap)
        {
            var transMapClone = BornNewMap();
            transMapClone.position = GetPosNewMap(isRight);
            if (isRight)
            {
                transMap.Add(transMapClone);
            }
            else
            {
                transMap.Insert(0, transMapClone);
            }
            transMapMiddle = transMap[2];
        }
    }

    private Transform BornNewMap()
    {
        var rand = Random.Range(0, transMapPrefabs.Count);
        var transMapClone = Instantiate(transMapPrefabs[rand], transMapParent);
        return transMapClone;
    }

    private Vector3 GetPosNewMap(bool isRight = true)
    {
        var result = new Vector3(widthScreen, 0, 0);
        if (isRight)
        {
            var finalMap = transMap[transMap.Count - 1].position;
            result = new Vector3(finalMap.x, finalMap.y, 0) + result;
        }
        else
        {
            var firstMap = transMap[0].position;
            result = new Vector3(firstMap.x, firstMap.y, 0) - result;
        }
        return result;
    }
}
