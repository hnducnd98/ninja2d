using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
    private static MainController _instance;
    public static MainController Instance { get { return _instance; } }

    [SerializeField] private Transform _transDeadZone;
    public Transform transDeadZone { get { return _transDeadZone; } }

    private Player _player;
    public Player player { get { return _player; } }

    private bool _isDead;
    public bool isDead
    {
        get { return _isDead; }
        set { _isDead = value; }
    }

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }

        if (_player == null)
        {
            _player = FindObjectOfType<Player>();
        }
    }
}
